package tk.thecolher.tagzz.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import tk.thecolher.tagzz.utils.JsonUtils;

public class Tag implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd,String arg, String[] args) {
		if (cmd.getLabel().equalsIgnoreCase("tag") && sender.hasPermission("tagzz.tagcommand")) {
			Player p = (Player) sender;
			if (arg.equalsIgnoreCase("set") && p.hasPermission("tagzz.set")) {
				if (args.equals(null)) {
					p.setDisplayName(p.getName());
					p.setPlayerListName(p.getName());
				}
				String perm = JsonUtils.getTag(args[0]).get(1);
				if (p.hasPermission(perm)) {
					String tag = JsonUtils.getTag(args[0]).get(0);
					String taggedp = tag+" "+p.getName();
					p.setDisplayName(taggedp);
					p.setPlayerListName(taggedp);
				}
			} else if (arg.equalsIgnoreCase("criar") && p.hasPermission("tagzz.criar") && !args.equals(null)) {
				if (!args[0].equals(null)&&!args[1].equals(null)&&!args[2].equals(null)) {
					JsonUtils.addTag(args[0], args[1], args[2]);
				}
			}
			return false;
		}
		return false;
	}

}
