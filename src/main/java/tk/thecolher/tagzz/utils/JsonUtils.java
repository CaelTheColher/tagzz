package tk.thecolher.tagzz.utils;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;





import java.util.List;

import tk.thecolher.tagzz.Main;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonUtils {
	static Gson json = new Gson();
    public static TagMap tagmap = new TagMap();
    public static HashMap<String, List<String>> map = new HashMap<>();
    public static void saveData() {
        tagmap = new TagMap();
        tagmap.tags = JsonUtils.map;
        try {
            Files.write(Main.tagsfile.toPath(), json.toJson(tagmap).getBytes(Charset.forName("UTF-8")));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static void loadData() {
        try {
            tagmap = json.fromJson(new String(Files.readAllBytes(Main.tagsfile.toPath()),Charset.forName("UTF-8")), TagMap.class);
        } catch (Exception e) {
            tagmap = new TagMap();
            saveData();
        }

        JsonUtils.map = tagmap.tags;
    }
    
	public static void addTag(String tagname,String tag, String permission) {
		List<String> lista = new ArrayList<String>();
		lista.add(tag);
		lista.add(permission);
		loadData();
		tagmap.tags.put(tagname, lista);
		saveData();
	}
	
	
	public static List<String> getTag(String tagname) {
		JsonParser parser = new JsonParser();
		try {
			JsonObject o = parser.parse(new String(Files.readAllBytes(Main.tagsfile.toPath()),Charset.forName("UTF-8"))).getAsJsonObject();
			o = (JsonObject) o.get("tags");
			JsonArray jarray = o.getAsJsonArray(tagname);
			List<String> lsti = new ArrayList<String>();
			jarray.forEach(s -> lsti.add(s.getAsString()));
			return lsti;
		} catch (Exception e) {
			List<String> lst = new ArrayList<String>();
			lst.add("");
			lst.add("notag");
			return lst;
		}
	}
}
