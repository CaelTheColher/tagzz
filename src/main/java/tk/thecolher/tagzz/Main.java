package tk.thecolher.tagzz;

import java.io.File;
import java.io.IOException;

import org.bukkit.plugin.java.JavaPlugin;

import tk.thecolher.tagzz.commands.Tag;

public class Main extends JavaPlugin {
	public static File tagsfile = new File(Main.getPlugin(Main.class).getDataFolder() + "tags.json");
	@Override
	public void onEnable() {
		this.getDataFolder().mkdir();
	    
	    try {
	        tagsfile.createNewFile();
	    } catch(IOException ioe) {
	        ioe.printStackTrace();
	    }
	    getCommand("tag").setExecutor(new Tag());
	}

}
